FROM lucee/lucee52:latest
# FROM lucee/lucee52-nginx:latest

LABEL maintainer="Michael Gatto <mgatto@vinsuite.com>"
LABEL Description="This container is for developer machines. It serves CFML from Lucee and saves sessions into memcached."
LABEL Vendor="vinSUITE"
LABEL Version="1.0"

EXPOSE 8080 8888

# NGINX configs
# COPY config/nginx/ /etc/nginx/

# Lucee server PRODUCTION configs
COPY config/lucee/lucee-server.xml /opt/lucee/server/lucee-server/context/lucee-server.xml
COPY config/lucee/lucee-web.xml.cfm /opt/lucee/web/lucee-web.xml.cfm

# Deploy codebase to container
VOLUME /var/www
# COPY project /var/www
