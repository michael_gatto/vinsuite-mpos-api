interface {
	public query function findAll(required struct criteria); // would be great if we could use Generics here...
	public any function findOne(any entity);
}
