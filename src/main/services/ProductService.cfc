<cfscript>
	/**
	 * ComponentName.cfc
	 *
	 * @hint provides access to product data and operates on product domain objects
	 * @author anonymous
	 * @date date
	 **/
	component implements="Service" accessors=true output=false {
		property Array products;
		property product;

		public ProductService function init(required Product product) {
			//this.productModel = product;

			return this;
		}


		public any function findOne(any entity) {

		}

		public query function findAll(required struct criteria = {}) {
			/* validation */
			// criteria must have at least a wineryId
			if (! structKeyExists(arguments.criteria, "wineryId")) {
				throw(type = "ProductService.BadRequest", message = "Required criteria member: 'wineryId' is missing", detail = serialize(arguments), errorCode = "401");
			}

			local.products = queryExecute("
					SELECT 	p.productId, p.ProductSKU, p.productName, p.photoPath, p.picture, CAST(p.price1 AS DECIMAL(18,2)) as price1, p.isTaxable, CAST(p.costperbottle AS DECIMAL(18,2)) as costperbottle
							, CAST(p.costpercase AS DECIMAL(18,2)) as costpercase
							, p.Teaser
							, w.Vintage, wv.WineVarietal, wa.WineAppellation
							, wr.WineRegion
							, wb.WineBrand
					FROM Products p
					LEFT OUTER JOIN Wines w
							INNER JOIN WineVarietals wv ON w.WineVarietalID = wv.WineVarietalID
								INNER JOIN WineAppellations wa ON w.WineAppellationID = wa.WineAppellationID
								ON w.WineID = p.productkeyID
					LEFT OUTER JOIN wineRegions wr
						ON w.WineRegionID = wr.WineRegionID
					LEFT OUTER JOIN winebrands wb
						ON w.WineBrandID = wb.WineBrandID
					LEFT OUTER JOIN ProductsXProductCategories pxpc
						ON p.productid = pxpc.productid
					LEFT OUTER JOIN ProductCategories pc
						ON pxpc.productcategoryid=pc.productcategoryid
					WHERE p.WineryID = :wineryid
						AND pc.ProductCategory = :productcategory
						AND p.isactive = 1
				", {
						wineryid = { value = arguments.criteria.wineryID, cfsqltype = "idstamp" },
						productcategory = { value = "POS STORE", cfsqltype = "string" }
					}
			);

			return local.products;
		}


	}
</cfscript>
