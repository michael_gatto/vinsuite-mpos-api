<cfscript>
	component extends="taffy.core.resource" taffy_uri="/products/{wineryId}" accessors=true output=false {
		// property type="ProductService" name="_productService";
		property ProductService;
			// autowire services/products.cfc or with omitDirectoryAliases will just look for the unique CFC ProductService
			/*
				When using property to declare a dependency, do not specify a type or a default: DI/1 assumes that typed properties (and defaulted properties) are intended to generate specific getters and setters on transients or for ORM integration, rather than just dependencies. You can override this default behavior - see Configuration below.
			 */

		/**
		 * @hint Constructor
		 *
		 */
		 public Products function init(required ProductService productservice) { // autowire services/product.cfc
	         variables.productservice = productservice;

	         return this;
	     }

		/**
		 * @hint Finds all products per a single Winery
		 *
		 * @wineryid.hint unique to each winery
		 * @wineryid.type uuid
		 * @wineryid.required true
		 *
		 */
		public any function getAllProducts(required string wineryId) taffy_verb="get" {
			try {
				if(! isValid("guid", arguments.wineryId) ) {
					throw(type = "ProductResource.BadRequest", message = "The wineryId is not a valid GUID", detail = serialize(arguments), errorCode = "401");
				}

				//variables.defaultProductCategory = "";

				local.products = variables.productservice.findAll({
					"wineryId": arguments.wineryId
					//, "productCategory": variables.defaultProductCategory
				});

				return representationOf(queryToArray(local.products)).withStatus(200);

			} catch(any e) {
				rethrow;
			}
		}

		/*
		public any function get() {}
		public any function post() {}
		public any function put() {}
		public any function delete() {}
		*/
	}
</cfscript>
