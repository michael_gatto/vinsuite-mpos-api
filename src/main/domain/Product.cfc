/**
 * ComponentName.cfc
 *
 * @author anonymous
 * @date date
 **/
component accessors=true output=false  {
	property string productId;
	property string productname;

	/**
	 * @hint Constructor to setup a Product domain object
	 * @return {[type]} [description]
	 */
	public Product function init() {
		return this;
	}


}
